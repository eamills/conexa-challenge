export function replaceUrls(
  obj: any,
  search: string,
  replacement: string,
): void {
  for (const key in obj) {
    if (typeof obj[key] === 'string') {
      obj[key] = obj[key].replace(search, replacement);
    } else if (typeof obj[key] === 'object') {
      replaceUrls(obj[key], search, replacement);
    }
  }
}
