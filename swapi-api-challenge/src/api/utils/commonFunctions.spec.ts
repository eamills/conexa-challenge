import { replaceUrls } from './commonFunctions';

describe('replaceUrls', () => {
  it('debería reemplazar las URLs en un objeto correctamente', () => {
    const inputObj = {
      key1: 'https://old-url.com',
      key2: 'Texto sin URL',
      nestedObj: {
        key3: 'https://another-old-url.com',
      },
    };

    const search = 'https://old-url.com';
    const replacement = 'https://new-url.com';

    replaceUrls(inputObj, search, replacement);

    expect(inputObj.key1).toBe('https://new-url.com');
    expect(inputObj.key2).toBe('Texto sin URL');
    expect(inputObj.nestedObj.key3).toBe('https://another-old-url.com'); // Debería permanecer sin cambios
  });

  it('no debería afectar las propiedades no string', () => {
    const inputObj = {
      key1: 123,
      key2: null,
      key3: true,
    };

    const search = 'old';
    const replacement = 'new';

    replaceUrls(inputObj, search, replacement);

    expect(inputObj.key1).toBe(123);
    expect(inputObj.key2).toBe(null);
    expect(inputObj.key3).toBe(true);
  });

  it('debería reemplazar las URLs en objetos anidados', () => {
    const inputObj = {
      key1: 'https://old-url.com',
      nestedObj: {
        key2: 'https://nested-old-url.com',
        nestedNestedObj: {
          key3: 'https://nested-nested-old-url.com',
        },
      },
    };

    const search = 'old';
    const replacement = 'new';

    replaceUrls(inputObj, search, replacement);

    expect(inputObj.key1).toBe('https://new-url.com');
    expect(inputObj.nestedObj.key2).toBe('https://nested-new-url.com');
    expect(inputObj.nestedObj.nestedNestedObj.key3).toBe(
      'https://nested-nested-new-url.com',
    );
  });
});
