import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ApiPersonController } from './controllers/api.person.controller';
import { ApiFilmsController } from './controllers/api.films.controller';
import { ApiStarshipsController } from './controllers/api.starships.controller';
import { ApiPlanetsController } from './controllers/api.planets.controller';
import { ApiPersonService } from './services/api.person.service';
import { ApiFilmsService } from './services/api.films.service';
import { ApiStarshipsService } from './services/api.starships.service';
import { ApiPlanetsService } from './services/api.planets.service';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    HttpModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
  ],
  controllers: [
    ApiPersonController,
    ApiFilmsController,
    ApiStarshipsController,
    ApiPlanetsController,
  ],
  providers: [
    ApiPersonService,
    ApiFilmsService,
    ApiStarshipsService,
    ApiPlanetsService,
  ],
})
export class ApiModule {}
