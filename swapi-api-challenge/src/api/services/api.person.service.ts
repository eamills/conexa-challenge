import { ForbiddenException, Injectable } from '@nestjs/common';
import { map, catchError } from 'rxjs';
import { HttpService } from '@nestjs/axios';
import { replaceUrls } from '../utils/commonFunctions';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class ApiPersonService {
  constructor(
    private http: HttpService,
    private configService: ConfigService,
  ) {}

  async getAllPeople(pageNumber?: number) {
    const url = this.configService.get<string>('APP_URL');
    const port = this.configService.get<number>('APP_PORT');
    const replacement = url + (port ? `:${port}` : '');
    const apiBaseURL = this.configService.get<string>('API_BASE_URL');
    const currentPage = pageNumber ? `/?page=${pageNumber}` : '';
    return this.http
      .get(`${apiBaseURL}/api/people${currentPage}`)
      .pipe(
        map((res) => {
          const data = res?.data;
          replaceUrls(data, apiBaseURL, replacement);
          return data;
        }),
      )
      .pipe(
        catchError(() => {
          throw new ForbiddenException('API not available');
        }),
      );
  }

  async getPerson(id: string) {
    const url = this.configService.get<string>('APP_URL');
    const port = this.configService.get<number>('APP_PORT');
    const replacement = url + (port ? `:${port}` : '');
    const apiBaseURL = this.configService.get<string>('API_BASE_URL');
    return this.http
      .get(`${apiBaseURL}/api/people/${id}`)
      .pipe(
        map((res) => {
          const data = res?.data;
          replaceUrls(data, apiBaseURL, replacement);
          return data;
        }),
      )
      .pipe(
        catchError(() => {
          throw new ForbiddenException('API not available');
        }),
      );
  }
}
