import { Test, TestingModule } from '@nestjs/testing';
import { ApiFilmsService } from './api.films.service';
import { HttpService, HttpModule } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { of, throwError } from 'rxjs';
import { ForbiddenException } from '@nestjs/common';
import { AxiosResponse } from 'axios';

describe('ApiFilmService', () => {
  let service: ApiFilmsService;
  let httpService: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        ApiFilmsService,
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn(() => 'mocked-api-base-url'), // Mocking the ConfigService
          },
        },
      ],
    }).compile();

    service = module.get<ApiFilmsService>(ApiFilmsService);
    httpService = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getAllFilms', () => {
    it('should retrieve all films', async () => {
      const apiBaseURL = 'mocked-api-base-url';
      const currentPage = '/?page=1';
      const responseData = { data: [{ id: '1', name: 'John Doe' }] };

      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(of(responseData as AxiosResponse));
      const resultPromise = (await service.getAllFilms(1)).toPromise(); // Convert to Promise

      await expect(resultPromise).resolves.toEqual(responseData.data);
      expect(httpService.get).toHaveBeenCalledWith(
        `${apiBaseURL}/api/films${currentPage}`,
      );
    });

    it('should throw ForbiddenException when API is not available', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(throwError(new Error()));

      const resultPromise = (await service.getAllFilms(1)).toPromise(); // Convert to Promise

      await expect(resultPromise).rejects.toThrowError(ForbiddenException);
    });
  });

  describe('getFilm', () => {
    it('should retrieve a film by ID', async () => {
      const apiBaseURL = 'mocked-api-base-url';
      const filmId = '1';
      const responseData = { data: { id: filmId, name: 'John Doe' } };

      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(of(responseData as AxiosResponse));

      const resultPromise = (await service.getFilm(filmId)).toPromise(); // Convert to Promise

      await expect(resultPromise).resolves.toEqual(responseData.data);

      expect(httpService.get).toHaveBeenCalledWith(
        `${apiBaseURL}/api/films/${filmId}`,
      );
    });

    it('should throw ForbiddenException when API is not available', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(throwError(new Error()));

      const resultPromise = (await service.getFilm('1')).toPromise(); // Convert to Promise
      await expect(resultPromise).rejects.toThrowError(ForbiddenException);
    });
  });
});
