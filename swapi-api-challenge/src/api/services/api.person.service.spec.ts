import { Test, TestingModule } from '@nestjs/testing';
import { ApiPersonService } from './api.person.service';
import { HttpService, HttpModule } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { of, throwError } from 'rxjs';
import { ForbiddenException } from '@nestjs/common';
import { AxiosResponse } from 'axios';

describe('ApiPersonService', () => {
  let service: ApiPersonService;
  let httpService: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        ApiPersonService,
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn(() => 'mocked-api-base-url'), // Mocking the ConfigService
          },
        },
      ],
    }).compile();

    service = module.get<ApiPersonService>(ApiPersonService);
    httpService = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getAllPeople', () => {
    it('should retrieve all people', async () => {
      const apiBaseURL = 'mocked-api-base-url';
      const currentPage = '/?page=1';
      const responseData = { data: [{ id: '1', name: 'John Doe' }] };

      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(of(responseData as AxiosResponse));
      const resultPromise = (await service.getAllPeople(1)).toPromise(); // Convert to Promise

      await expect(resultPromise).resolves.toEqual(responseData.data);
      expect(httpService.get).toHaveBeenCalledWith(
        `${apiBaseURL}/api/people${currentPage}`,
      );
    });

    it('should throw ForbiddenException when API is not available', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(throwError(new Error()));

      const resultPromise = (await service.getAllPeople(1)).toPromise(); // Convert to Promise

      await expect(resultPromise).rejects.toThrowError(ForbiddenException);
    });
  });

  describe('getPerson', () => {
    it('should retrieve a person by ID', async () => {
      const apiBaseURL = 'mocked-api-base-url';
      const personId = '1';
      const responseData = { data: { id: personId, name: 'John Doe' } };

      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(of(responseData as AxiosResponse));

      const resultPromise = (await service.getPerson(personId)).toPromise(); // Convert to Promise

      await expect(resultPromise).resolves.toEqual(responseData.data);

      expect(httpService.get).toHaveBeenCalledWith(
        `${apiBaseURL}/api/people/${personId}`,
      );
    });

    it('should throw ForbiddenException when API is not available', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(throwError(new Error()));

      const resultPromise = (await service.getPerson('1')).toPromise(); // Convert to Promise
      await expect(resultPromise).rejects.toThrowError(ForbiddenException);
    });
  });
});
