import { Test, TestingModule } from '@nestjs/testing';
import { ApiPlanetsService } from './api.planets.service';
import { HttpService, HttpModule } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { of, throwError } from 'rxjs';
import { ForbiddenException } from '@nestjs/common';
import { AxiosResponse } from 'axios';

describe('ApiPlanetsService', () => {
  let service: ApiPlanetsService;
  let httpService: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        ApiPlanetsService,
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn(() => 'mocked-api-base-url'), // Mocking the ConfigService
          },
        },
      ],
    }).compile();

    service = module.get<ApiPlanetsService>(ApiPlanetsService);
    httpService = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getAllPlanets', () => {
    it('should retrieve all planets', async () => {
      const apiBaseURL = 'mocked-api-base-url';
      const currentPage = '/?page=1';
      const responseData = { data: [{ id: '1', name: 'John Doe' }] };

      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(of(responseData as AxiosResponse));
      const resultPromise = (await service.getAllPlanets(1)).toPromise(); // Convert to Promise

      await expect(resultPromise).resolves.toEqual(responseData.data);
      expect(httpService.get).toHaveBeenCalledWith(
        `${apiBaseURL}/api/planets${currentPage}`,
      );
    });

    it('should throw ForbiddenException when API is not available', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(throwError(new Error()));

      const resultPromise = (await service.getAllPlanets(1)).toPromise(); // Convert to Promise

      await expect(resultPromise).rejects.toThrowError(ForbiddenException);
    });
  });

  describe('getPlanet', () => {
    it('should retrieve a planet by ID', async () => {
      const apiBaseURL = 'mocked-api-base-url';
      const planetId = '1';
      const responseData = { data: { id: planetId, name: 'John Doe' } };

      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(of(responseData as AxiosResponse));

      const resultPromise = (await service.getPlanet(planetId)).toPromise(); // Convert to Promise

      await expect(resultPromise).resolves.toEqual(responseData.data);

      expect(httpService.get).toHaveBeenCalledWith(
        `${apiBaseURL}/api/planets/${planetId}`,
      );
    });

    it('should throw ForbiddenException when API is not available', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(throwError(new Error()));

      const resultPromise = (await service.getPlanet('1')).toPromise(); // Convert to Promise
      await expect(resultPromise).rejects.toThrowError(ForbiddenException);
    });
  });
});
