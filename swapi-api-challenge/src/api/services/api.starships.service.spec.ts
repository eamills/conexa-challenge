import { Test, TestingModule } from '@nestjs/testing';
import { ApiStarshipsService } from './api.starships.service';
import { HttpService, HttpModule } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { of, throwError } from 'rxjs';
import { ForbiddenException } from '@nestjs/common';
import { AxiosResponse } from 'axios';

describe('ApiStarshipsService', () => {
  let service: ApiStarshipsService;
  let httpService: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        ApiStarshipsService,
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn(() => 'mocked-api-base-url'), // Mocking the ConfigService
          },
        },
      ],
    }).compile();

    service = module.get<ApiStarshipsService>(ApiStarshipsService);
    httpService = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getAllStarships', () => {
    it('should retrieve all starships', async () => {
      const apiBaseURL = 'mocked-api-base-url';
      const currentPage = '/?page=1';
      const responseData = { data: [{ id: '1', name: 'John Doe' }] };

      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(of(responseData as AxiosResponse));
      const resultPromise = (await service.getAllStarships(1)).toPromise(); // Convert to Promise

      await expect(resultPromise).resolves.toEqual(responseData.data);
      expect(httpService.get).toHaveBeenCalledWith(
        `${apiBaseURL}/api/starships${currentPage}`,
      );
    });

    it('should throw ForbiddenException when API is not available', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(throwError(new Error()));

      const resultPromise = (await service.getAllStarships(1)).toPromise(); // Convert to Promise

      await expect(resultPromise).rejects.toThrowError(ForbiddenException);
    });
  });

  describe('getStarship', () => {
    it('should retrieve a starship by ID', async () => {
      const apiBaseURL = 'mocked-api-base-url';
      const starshipId = '1';
      const responseData = { data: { id: starshipId, name: 'John Doe' } };

      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(of(responseData as AxiosResponse));

      const resultPromise = (await service.getStarship(starshipId)).toPromise(); // Convert to Promise

      await expect(resultPromise).resolves.toEqual(responseData.data);

      expect(httpService.get).toHaveBeenCalledWith(
        `${apiBaseURL}/api/starships/${starshipId}`,
      );
    });

    it('should throw ForbiddenException when API is not available', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockReturnValueOnce(throwError(new Error()));

      const resultPromise = (await service.getStarship('1')).toPromise(); // Convert to Promise
      await expect(resultPromise).rejects.toThrowError(ForbiddenException);
    });
  });
});
