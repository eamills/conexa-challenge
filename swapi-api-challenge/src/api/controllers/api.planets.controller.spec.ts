import { Test, TestingModule } from '@nestjs/testing';
import { ApiPlanetsController } from './api.planets.controller';
import { ApiPlanetsService } from '../services/api.planets.service';
import { of } from 'rxjs';
import { HttpModule } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';

describe('ApiFilmsController', () => {
  let controller: ApiPlanetsController;
  let apiService: ApiPlanetsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [ApiPlanetsController],
      providers: [
        ApiPlanetsService,
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn(() => 'mocked-api-base-url'), // Mocking the ConfigService
          },
        },
      ],
    }).compile();

    controller = module.get<ApiPlanetsController>(ApiPlanetsController);
    apiService = module.get<ApiPlanetsService>(ApiPlanetsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getAllPlanets', () => {
    it('should return an array of films', async () => {
      const films = [
        { id: 1, name: 'p 1' },
        { id: 2, name: 'p 2' },
      ];
      jest.spyOn(apiService, 'getAllPlanets').mockReturnValue(of(films) as any);

      const query = { page: 1 };
      const result = await (await controller.getAllPlanets(query)).toPromise();
      expect(result).toEqual(films);
    });
  });

  describe('getPlanet', () => {
    it('should return a film', async () => {
      const film = { id: 1, name: 'p 1' };
      jest.spyOn(apiService, 'getPlanet').mockReturnValue(of(film) as any);

      const id = '1';
      const result = await (await controller.getPlanet(id)).toPromise();
      expect(result).toEqual(film);
    });
  });
});
