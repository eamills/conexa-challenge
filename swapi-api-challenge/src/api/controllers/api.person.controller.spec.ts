import { Test, TestingModule } from '@nestjs/testing';
import { ApiPersonController } from './api.person.controller';
import { ApiPersonService } from '../services/api.person.service';
import { of } from 'rxjs';
import { HttpModule } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';

describe('ApiFilmsController', () => {
  let controller: ApiPersonController;
  let apiService: ApiPersonService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [ApiPersonController],
      providers: [
        ApiPersonService,
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn(() => 'mocked-api-base-url'), // Mocking the ConfigService
          },
        },
      ],
    }).compile();

    controller = module.get<ApiPersonController>(ApiPersonController);
    apiService = module.get<ApiPersonService>(ApiPersonService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getAllPeople', () => {
    it('should return an array of films', async () => {
      const films = [
        { id: 1, name: 'p 1' },
        { id: 2, name: 'p 2' },
      ];
      jest.spyOn(apiService, 'getAllPeople').mockReturnValue(of(films) as any);

      const query = { page: 1 };
      const result = await (await controller.getAllPeople(query)).toPromise();
      expect(result).toEqual(films);
    });
  });

  describe('getPerson', () => {
    it('should return a film', async () => {
      const film = { id: 1, name: 'p 1' };
      jest.spyOn(apiService, 'getPerson').mockReturnValue(of(film) as any);

      const id = '1';
      const result = await (await controller.getPerson(id)).toPromise();
      expect(result).toEqual(film);
    });
  });
});
