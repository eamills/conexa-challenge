import { Controller, Get, Param, Query, Header } from '@nestjs/common';
import { ApiFilmsService } from '../services/api.films.service';

@Controller('api/films')
export class ApiFilmsController {
  constructor(private apiService: ApiFilmsService) {}

  @Get()
  @Header('Access-Control-Allow-Origin', '*')
  getAllFilms(@Query() query: any) {
    const pageNumber = query?.page ? parseInt(query.page) : null;
    return this.apiService.getAllFilms(pageNumber);
  }

  @Get(':id')
  @Header('Access-Control-Allow-Origin', '*')
  getFilms(@Param('id') id: string) {
    return this.apiService.getFilm(id);
  }
}
