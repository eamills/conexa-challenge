import { Controller, Get, Param, Query, Header } from '@nestjs/common';
import { ApiPersonService } from '../services/api.person.service';

@Controller('api/people')
export class ApiPersonController {
  constructor(private apiService: ApiPersonService) {}

  @Get()
  @Header('Access-Control-Allow-Origin', '*')
  getAllPeople(@Query() query: any) {
    const pageNumber = query?.page ? parseInt(query.page) : null;
    return this.apiService.getAllPeople(pageNumber);
  }

  @Get(':id')
  @Header('Access-Control-Allow-Origin', '*')
  getPerson(@Param('id') id: string) {
    return this.apiService.getPerson(id);
  }
}
