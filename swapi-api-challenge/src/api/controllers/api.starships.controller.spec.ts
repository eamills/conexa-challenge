import { Test, TestingModule } from '@nestjs/testing';
import { ApiStarshipsController } from './api.starships.controller';
import { ApiStarshipsService } from '../services/api.starships.service';
import { of } from 'rxjs';
import { HttpModule } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';

describe('ApiFilmsController', () => {
  let controller: ApiStarshipsController;
  let apiService: ApiStarshipsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [ApiStarshipsController],
      providers: [
        ApiStarshipsService,
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn(() => 'mocked-api-base-url'), // Mocking the ConfigService
          },
        },
      ],
    }).compile();

    controller = module.get<ApiStarshipsController>(ApiStarshipsController);
    apiService = module.get<ApiStarshipsService>(ApiStarshipsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getAllStarships', () => {
    it('should return an array of films', async () => {
      const films = [
        { id: 1, name: 'p 1' },
        { id: 2, name: 'p 2' },
      ];
      jest
        .spyOn(apiService, 'getAllStarships')
        .mockReturnValue(of(films) as any);

      const query = { page: 1 };
      const result = await (
        await controller.getAllStarships(query)
      ).toPromise();
      expect(result).toEqual(films);
    });
  });

  describe('getStarship', () => {
    it('should return a film', async () => {
      const film = { id: 1, name: 'p 1' };
      jest.spyOn(apiService, 'getStarship').mockReturnValue(of(film) as any);

      const id = '1';
      const result = await (await controller.getStarship(id)).toPromise();
      expect(result).toEqual(film);
    });
  });
});
