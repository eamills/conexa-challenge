import { Test, TestingModule } from '@nestjs/testing';
import { ApiFilmsController } from './api.films.controller';
import { ApiFilmsService } from '../services/api.films.service';
import { of } from 'rxjs';
import { HttpModule } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';

describe('ApiFilmsController', () => {
  let controller: ApiFilmsController;
  let apiService: ApiFilmsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [ApiFilmsController],
      providers: [
        ApiFilmsService,
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn(() => 'mocked-api-base-url'), // Mocking the ConfigService
          },
        },
      ],
    }).compile();

    controller = module.get<ApiFilmsController>(ApiFilmsController);
    apiService = module.get<ApiFilmsService>(ApiFilmsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getAllFilms', () => {
    it('should return an array of films', async () => {
      const films = [
        { id: 1, title: 'Film 1' },
        { id: 2, title: 'Film 2' },
      ];
      jest.spyOn(apiService, 'getAllFilms').mockReturnValue(of(films) as any);

      const query = { page: 1 };
      const result = await (await controller.getAllFilms(query)).toPromise();
      expect(result).toEqual(films);
    });
  });

  describe('getFilm', () => {
    it('should return a film', async () => {
      const film = { id: 1, title: 'Film 1' };
      jest.spyOn(apiService, 'getFilm').mockReturnValue(of(film) as any);

      const id = '1';
      const result = await (await controller.getFilms(id)).toPromise();
      expect(result).toEqual(film);
    });
  });
});
