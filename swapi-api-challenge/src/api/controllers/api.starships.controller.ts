import { Controller, Get, Param, Query, Header } from '@nestjs/common';
import { ApiStarshipsService } from '../services/api.starships.service';

@Controller('api/starships')
export class ApiStarshipsController {
  constructor(private apiService: ApiStarshipsService) {}

  @Get()
  @Header('Access-Control-Allow-Origin', '*')
  getAllStarships(@Query() query: any) {
    const pageNumber = query?.page ? parseInt(query.page) : null;
    return this.apiService.getAllStarships(pageNumber);
  }

  @Get(':id')
  @Header('Access-Control-Allow-Origin', '*')
  getStarship(@Param('id') id: string) {
    return this.apiService.getStarship(id);
  }
}
