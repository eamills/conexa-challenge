import { Controller, Get, Param, Query, Header } from '@nestjs/common';
import { ApiPlanetsService } from '../services/api.planets.service';

@Controller('api/planets')
export class ApiPlanetsController {
  constructor(private apiService: ApiPlanetsService) {}

  @Get()
  @Header('Access-Control-Allow-Origin', '*')
  getAllPlanets(@Query() query: any) {
    const pageNumber = query?.page ? parseInt(query.page) : null;
    return this.apiService.getAllPlanets(pageNumber);
  }

  @Get(':id')
  @Header('Access-Control-Allow-Origin', '*')
  getPlanet(@Param('id') id: string) {
    return this.apiService.getPlanet(id);
  }
}
