# Proyecto NestJS y ReactJS con Docker Compose

Este proyecto incluye dos aplicaciones: un backend Node.js con NestJS y un frontend ReactJS, ambos configurados para ejecutarse en contenedores Docker.

## Instrucciones de Uso

Asegúrate de tener Docker instalado en tu máquina antes de continuar.

1. Clona este repositorio:

```
$ git clone https://github.com/tu-usuario/tu-proyecto.git
$ cd conexa-challenge
``````

2. Construye y levanta los contenedores usando Docker Compose:

```
$ docker-compose up
```
Si no tienes instalado docker o docker compose puedes averiguar cómo hacerlo en este enlace https://docs.docker.com/compose/

3. Una vez que los contenedores estén en funcionamiento, puedes acceder a las aplicaciones desde tu navegador web:

```
- Aplicación NestJS: http://localhost:3000
- Aplicación ReactJS: http://localhost:8088
```


4. Cuando hayas terminado, puedes detener los contenedores presionando `Ctrl + C` en la terminal donde ejecutaste `docker-compose up`.


## Microservicios

En este repositorio hay una colección para postman para la prueba y ejecución de los microservicios.
Puedes importarlo siguiendo la siguiente documentación https://learning.postman.com/docs/collections/using-collections/

## Ejecución sin Docker

Si no cuentas con Docker o no es posible usar docker-compose sigue las siguientes intrucciones para ejecutar por separado cada proyecto.

1. Ir a la carpeta raiz del proyecto
2. Ejecutar en consola
```
$ cd swapi-api-challenge
$ npm ci
$ npm start
```

3. Despues de ejecutar el comando, en otra consola ir a la carpeta raiz y ejecutar:
```
$ cd swapi-ui-challenge
$ npm ci
$npm start
```
Esto deberia ejecutar la aplicación react en el puerto 3333 sin embargo, si tienes algo corriendo ahi, el mismo programa recomendará otro puerto.
