const romanizeNumber = (num: number) => {
	const lookup = ['', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'VIII'];
	return lookup[num];
};
export default romanizeNumber;
