const baseURL = 'http://localhost:3000'
const API_MOVIE = `${baseURL}/api/films/`;
const API_CHARACTER = `${baseURL}/api/people/`;
const API_STARSHIP = `${baseURL}/api/starships/`;
const API_PLANET = `${baseURL}/api/planets/`;

export { API_MOVIE, API_CHARACTER, API_STARSHIP, API_PLANET };
