import React from 'react';
import { useNavigationType } from 'react-router-dom';

import Charactermovies from './lists/Charactermovieslist';
import BackButton from './ui/BackButton';
import { Planet as IPlanet } from '../ts/interfaces';

type Props = {
	planet: IPlanet;
};

const Planetmain: React.FC<Props> = ({ planet }) => {
	let navtype = useNavigationType();

	return (
		<>
			<div className="p-4">
				{navtype === 'PUSH' && <BackButton />}
				<h1 className="mt-4 pb-2 text-4xl font-bold">{planet.name}</h1>
				<dl className="char-dl pt-4 pb-2">
					<dt className="char-dt">Rotation period</dt>
					<dd className="char-dd">{planet.rotation_period}</dd>
					<dt className="char-dt">Orbital Period</dt>
					<dd className="char-dd">{planet.orbital_period}</dd>
					<dt className="char-dt">Diameter</dt>
					<dd className="char-dd">{planet.diameter}</dd>
					<dt className="char-dt">Climate</dt>
					<dd className="char-dd">{planet.climate}</dd>
					<dt className="char-dt">Gravity</dt>
					<dd className="char-dd">{planet.gravity}</dd>
					<dt className="char-dt">Terrain</dt>
					<dd className="char-dd">{planet.terrain}</dd>
					<dt className="char-dt">Surface water</dt>
					<dd className="char-dd">{planet.surface_water}</dd>
					<dt className="char-dt">Population</dt>
					<dd className="char-dd">{planet.population}</dd>
				</dl>
			</div>
			<section>
				<h3 className="px-4 pt-2 text-xl font-bold text-zinc-600">
					Appearances
				</h3>
				<Charactermovies movies={planet.films}></Charactermovies>
			</section>
		</>
	);
};

export default Planetmain;
