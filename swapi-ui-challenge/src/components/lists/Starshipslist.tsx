import { API_STARSHIP } from '../../api/endpoints';
import useFetch from '../../hooks/useFetch';
import Cardwrapper from '../ui/Cardwrapper';
import Card from '../ui/Card';
import Cardcharacter from '../ui/Cardcharacter';
import { Loader } from '../ui/Loader';
import simpleKeyFromUrl from '../../utils/getSimpleKey';
import getResourceId from '../../utils/getResourceId';
import { Starship } from '../../ts/interfaces';

const Starshiplist: React.FC = () => {
	const { response, loading } = useFetch(API_STARSHIP);

	return (
		<>
			{loading && <Loader />}
			{response && response.length > 0 && (
				<>
					<h1 className="p-4 pb-2 text-4xl font-extrabold text-zinc-700">
						Starships
					</h1>
					<Cardwrapper>
						{response.map((starship: Starship) => {
							const key = simpleKeyFromUrl(starship.url.toString());
							const characterId = getResourceId(starship.url);
							return (
								<Card key={key}>
									<Cardcharacter
										name={starship.name}
										url={characterId ? `/starships/${characterId}` : '#'}
									/>
								</Card>
							);
						})}
					</Cardwrapper>
				</>
			)}
		</>
	);
};

export default Starshiplist;
