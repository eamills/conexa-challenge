import { API_PLANET } from '../../api/endpoints';
import useFetch from '../../hooks/useFetch';
import Cardwrapper from '../ui/Cardwrapper';
import Card from '../ui/Card';
import Cardcharacter from '../ui/Cardcharacter';
import { Loader } from '../ui/Loader';
import simpleKeyFromUrl from '../../utils/getSimpleKey';
import getResourceId from '../../utils/getResourceId';
import { Planet } from '../../ts/interfaces';

const PlanetList: React.FC = () => {
	const { response, loading } = useFetch(API_PLANET);

	return (
		<>
			{loading && <Loader />}
			{response && response.length > 0 && (
				<>
					<h1 className="p-4 pb-2 text-4xl font-extrabold text-zinc-700">
						Planets
					</h1>
					<Cardwrapper>
						{response.map((planet: Planet) => {
							const key = simpleKeyFromUrl(planet.url.toString());
							const characterId = getResourceId(planet.url);
							return (
								<Card key={key}>
									<Cardcharacter
										name={planet.name}
										url={characterId ? `/planets/${characterId}` : '#'}
									/>
								</Card>
							);
						})}
					</Cardwrapper>
				</>
			)}
		</>
	);
};

export default PlanetList;
