import React from 'react';
import { useNavigationType } from 'react-router-dom';

import Charactermovies from './lists/Charactermovieslist';
import BackButton from './ui/BackButton';
import { Starship as IStaship } from '../ts/interfaces';

type Props = {
	starship: IStaship;
};

const Starshipmain: React.FC<Props> = ({ starship }) => {
	let navtype = useNavigationType();

	return (
		<>
			<div className="p-4">
				{navtype === 'PUSH' && <BackButton />}
				<h1 className="mt-4 pb-2 text-4xl font-bold">{starship.name}</h1>
				<dl className="char-dl pt-4 pb-2">
					<dt className="char-dt">Model</dt>
					<dd className="char-dd">{starship.model}</dd>
					<dt className="char-dt">Manofacturer</dt>
					<dd className="char-dd">{starship.manufacturer}</dd>
					<dt className="char-dt">Cost in credits</dt>
					<dd className="char-dd">{starship.cost_in_credits}</dd>
					<dt className="char-dt">Lenght</dt>
					<dd className="char-dd">{starship.length}</dd>
					<dt className="char-dt">Max Atmosphering Speed</dt>
					<dd className="char-dd">{starship.max_atmosphering_speed}</dd>
					<dt className="char-dt">Crew</dt>
					<dd className="char-dd">{starship.crew}</dd>
					<dt className="char-dt">Passengers</dt>
					<dd className="char-dd">{starship.passengers}</dd>
					<dt className="char-dt">Cargo capacity</dt>
					<dd className="char-dd">{starship.cargo_capacity}</dd>
					<dt className="char-dt">Consumables</dt>
					<dd className="char-dd">{starship.consumables}</dd>
					<dt className="char-dt">Hyperdrive rating</dt>
					<dd className="char-dd">{starship.hyperdrive_rating}</dd>
					<dt className="char-dt">MGLT</dt>
					<dd className="char-dd">{starship.MGLT}</dd>
					<dt className="char-dt">Starship Class</dt>
					<dd className="char-dd">{starship.starship_class}</dd>
				</dl>
			</div>
			<section>
				<h3 className="px-4 pt-2 text-xl font-bold text-zinc-600">
					Appearances
				</h3>
				<Charactermovies movies={starship.films}></Charactermovies>
			</section>
		</>
	);
};

export default Starshipmain;
