import { useParams } from 'react-router-dom';
import { API_PLANET } from '../api/endpoints';
import Planetmain from '../components/PlanetMain';
import { Loader } from '../components/ui/Loader';
import useFetch from '../hooks/useFetch';

export default function Planet() {
	const { id: planetId } = useParams();

	const { response, loading } = useFetch(
		`${API_PLANET}${planetId}`
	);

	return (
		<>
			{loading && <Loader />}
			{response && <Planetmain planet={response} />}
		</>
	);
}
