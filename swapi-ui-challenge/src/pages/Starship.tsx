import { useParams } from 'react-router-dom';
import { API_STARSHIP } from '../api/endpoints';
import Starshipmain from '../components/StarshipMain';
import { Loader } from '../components/ui/Loader';
import useFetch from '../hooks/useFetch';

export default function Starship() {
	const { id: starshipId } = useParams();

	const { response, loading } = useFetch(
		`${API_STARSHIP}${starshipId}`
	);

	return (
		<>
			{loading && <Loader />}
			{response && <Starshipmain starship={response} />}
		</>
	);
}
