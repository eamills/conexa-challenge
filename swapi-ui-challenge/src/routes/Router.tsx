import { Routes, Route } from 'react-router-dom';
import Home from '../pages/Home';
import People from '../pages/People';
import Character from '../pages/Character';
import Movies from '../pages/Movies';
import Movie from '../pages/Movie';
import Starships from '../pages/Starships'
import Starship from '../pages/Starship';
import Planets from '../pages/Planets';
import Planet from '../pages/Planet';

const Router = () => {
	return (
		<Routes>
			<Route path="/" element={<Home />} />
			<Route path="people" element={<People />} />
			<Route path="people/:id" element={<Character />} />
			<Route path="movies" element={<Movies />} />			
			<Route path="movies/:id" element={<Movie />} />
			<Route path="starships" element={<Starships />} />
			<Route path="starships/:id" element={<Starship />} />
			<Route path="planets" element={<Planets />} />
			<Route path="planets/:id" element={<Planet />} />
		</Routes>
	);
};

export default Router;
